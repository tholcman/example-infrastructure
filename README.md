# Interview project

Let's say this is a terraform description of infrastructure for `app-1`. Application deployment is described in kubernetes manifests in another git repository. 

> Note: This repo provides permissions to deploy into specific namespace via `gitlab_project_cluster` in `ns-app-1.tf` 

## Tasks:

> It doesn't have to be implemented in code, just described with some notes, so we can discuss it during interview. 

1. Check the code, write down what do you like or don't like about it and what are potential issues (there are some intentionally).

2. Let's say the infrastructure is deployed, the devs have their app and pipeline ready, but the app doesn't work. The developer will write you: "It's screaming something about bucket permission!" Can you find the issue? How would you fix that? 

3. Let's say company around this project will grow, and you will have to start cooperate with another infrastructure engineer. Do you see some necessary changes or nice to have improvements to the code?

4. There will come a requirement that we want to host another application `app-2` in the same cluster, and it seems like more apps will come. Approach probably would be to copy end edit the `ns-app-1.tf` for new application.
    - are there any obstacles with that? what are other information you need from devs?
    - the new app will need a DB (different one), but to save some expenses we would like to use same DB instance. Try to design new terraform module for it - it is enough to write down parameters and list of resources it will need.
