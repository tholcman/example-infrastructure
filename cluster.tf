module "gke_cluster" {
  source                            = "terraform-google-modules/kubernetes-engine/google"
  project_id                        = var.GCP_PROJECT_ID
  name                              = "cluster-${var.GCP_REGION}"
  region                            = var.GCP_REGION
  zones                             = [var.GCP_DEFAULT_ZONE]
  http_load_balancing               = true
  horizontal_pod_autoscaling        = true
  release_channel                   = "REGULAR"
  disable_legacy_metadata_endpoints = true
  network                           = "screening"
  subnetwork                        = "screening-us-central1"
  ip_range_pods                     = "screening-us-central1-pods"
  ip_range_services                 = "screening-us-central1-services"

  node_pools = [
    {
      name               = "general-20210122"
      machine_type       = "e2-medium"
      min_count          = 2
      max_count          = 5
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 2
    },
  ]
}

