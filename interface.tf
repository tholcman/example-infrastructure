variable "APP_GITLAB_PROJECT_ID" {}
variable "GCP_PROJECT_ID" {}
variable "GCP_REGION" {}
variable "GCP_DEFAULT_ZONE" {}
variable "GITLAB_TOKEN" {}
variable "REGISTRY_USERNAME" {}
variable "REGISTRY_TOKEN" {}
