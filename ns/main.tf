variable "name" {}
variable "registry_username" {}
variable "registry_token" {}

resource "kubernetes_namespace" "ns" {
  metadata {
    name = var.name
  }
}

resource "kubernetes_secret" "gitlab_docker_registry_credentials" {
  metadata {
    name      = "gitlab"
    namespace = kubernetes_namespace.ns.metadata[0].name
  }

  data = {
    ".dockercfg" = "{\"registry.gitlab.com\":{\"username\":\"${var.registry_username}\",\"password\":\"${var.registry_token}\"}}"
  }

  type = "kubernetes.io/dockercfg"
}

resource "kubernetes_service_account" "ci_deploy" {
  metadata {
    name      = "ci-deploy-user"
    namespace = kubernetes_namespace.ns.metadata[0].name
  }
}

data "kubernetes_secret" "ci_deploy_token" {
  metadata {
    name      = kubernetes_service_account.ci_deploy.default_secret_name
    namespace = kubernetes_service_account.ci_deploy.metadata.0.namespace
  }
}

output "deploy_token" {
  value = data.kubernetes_secret.ci_deploy_token.data["token"]
}

resource "kubernetes_role" "ci_deploy" {
  metadata {
    name      = "ci-deploy-user"
    namespace = kubernetes_namespace.ns.metadata[0].name
  }

  rule {
    api_groups = [
    "*"]
    resources = [
    "*"]
    verbs = [
    "*"]
  }
}

resource "kubernetes_role_binding" "ci_deploy_k8s" {
  metadata {
    name      = "ci-deploy-user-binding-k8s"
    namespace = kubernetes_namespace.ns.metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "ci-deploy-user"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.ci_deploy.metadata[0].name
    namespace = kubernetes_namespace.ns.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "ci_deploy_k8s" {
  metadata {
    name = "ci-deploy-user-k8s-${kubernetes_namespace.ns.metadata[0].name}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "ci-deploy-user" // created in main infra
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.ci_deploy.metadata[0].name
    namespace = kubernetes_namespace.ns.metadata[0].name
  }
}
