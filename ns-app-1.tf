module "ns_app_1" {
  source = "./ns"

  name              = "app-1"
  registry_username = var.REGISTRY_USERNAME
  registry_token    = var.REGISTRY_TOKEN
}


resource "gitlab_project_cluster" "gitlab_cluster" {
  project            = var.APP_GITLAB_PROJECT_ID
  name               = module.gke_cluster.name
  managed            = false
  kubernetes_api_url = "https://${module.gke_cluster.endpoint}"

  // valid only for limited time after terraform apply - definitely not good for gitlab kubernetes integration
  kubernetes_token     = module.ns_app_1.deploy_token
  kubernetes_ca_cert   = base64decode(module.gke_cluster.ca_certificate)
  kubernetes_namespace = "default"
  # kubernetes_authorization_type = "rbac"
  environment_scope = "app-1-*"
}

resource "kubernetes_secret" "app_1_db" {
  metadata {
    name      = "app-1-db"
    namespace = "app-1"
  }
  data = {
    "DB"     = google_sql_database.screening.name
    "DBUSER" = google_sql_user.screening.name
    "DBPASS" = random_password.db_password.result
    "DBHOST" = google_sql_database_instance.screening.private_ip_address
    "DBPORT" = "5432"
  }
}

resource "kubernetes_config_map" "api_config" {
  metadata {
    name      = "app-1-api"
    namespace = "app-1"
  }
  data = {
    BUCKET_NAME = google_storage_bucket.screening.name
  }
}
