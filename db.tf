resource "random_string" "db_hash" {
  length  = 6
  upper   = false
  special = false
}

resource "google_sql_database_instance" "screening" {
  depends_on          = [google_service_networking_connection.private_vpc_connection]
  name                = "screening-${random_string.db_hash.result}"
  region              = var.GCP_REGION
  database_version    = "POSTGRES_12"
  deletion_protection = false

  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled    = true
      private_network = module.screening_network.network_self_link
    }
  }
}

resource "google_sql_database" "screening" {
  name     = "screening"
  instance = google_sql_database_instance.screening.name
}

resource "random_password" "db_password" {
  length = 10
}

resource "google_sql_user" "screening" {
  name     = "screening"
  password = random_password.db_password.result
  instance = google_sql_database_instance.screening.name
}
