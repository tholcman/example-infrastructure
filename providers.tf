provider "google" {
  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION
}

provider "google-beta" {
  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION
}

data "google_client_config" "current" {}

provider "kubernetes" {
  load_config_file       = false
  host                   = "https://${module.gke_cluster.endpoint}"
  token                  = data.google_client_config.current.access_token
  cluster_ca_certificate = base64decode(module.gke_cluster.ca_certificate)
}

provider "gitlab" {
  token = var.GITLAB_TOKEN
}
