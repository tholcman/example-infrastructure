resource "random_pet" "bucket_name" {}

resource "google_storage_bucket" "screening" {
  name     = "l01-screening-${random_pet.bucket_name.id}"
  location = var.GCP_REGION
}

locals {
  files_count = 3
}

resource "random_pet" "files" {
  count = local.files_count
}

resource "google_storage_bucket_object" "screeining_test_file" {
  count   = local.files_count
  bucket  = google_storage_bucket.screening.name
  name    = "test-file-${random_pet.files[count.index].id}"
  content = "file-content ${random_pet.files[count.index].id}"
}
