module "screening_network" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 2.5"
  project_id   = var.GCP_PROJECT_ID
  network_name = "screening"

  subnets = [
    {
      subnet_name           = "screening-${var.GCP_REGION}"
      subnet_ip             = "10.0.0.0/23"
      subnet_region         = var.GCP_REGION
      subnet_private_access = "true"
    },
  ]

  secondary_ranges = {
    "screening-${var.GCP_REGION}" = [
      {
        range_name    = "screening-${var.GCP_REGION}-pods"
        ip_cidr_range = "10.64.0.0/18"
      },
      {
        range_name    = "screening-${var.GCP_REGION}-services"
        ip_cidr_range = "172.16.0.0/21"
      },
    ]
  }
}

resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = module.screening_network.network_self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = module.screening_network.network_self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_compute_firewall" "ssh" {
  name    = "ssh"
  network = module.screening_network.network_self_link

  source_ranges = [
    "0.0.0.0/0"
  ]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}
